import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
/*void main() => runApp(new MyApp());

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(title: 'Welcome to Flutter', home: new Scaffold(appBar: new AppBar(title: new Text('Bem vindo ao Flutter')), body: new Center(child: new Text('Hello World'))));
  }
}*/

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  //run each time the MaterialApp requires rendering, or when toggling the Platform in Flutter Inspector.
  Widget build(BuildContext context) {
    //final wordPair = WordPair.random();
    return new MaterialApp(
      title: 'Bem vindo ao Flutter',
      theme: ThemeData(
        primaryColor: Colors.white
      ),
      home: RandomWords(),
      /*home: new Scaffold(
        appBar: new AppBar(
          title: new Text('Welcome to Flutter'),
        ),
        body: new Center(
          //child: new Text('Hello World'),
          //child: Text(wordPair.asPascalCase),
          child: RandomWords(),
        ),
      ),*/
    );
  }
}

/*Stateless widgets are immutable, meaning that their properties can’t change—all values are final.

Stateful widgets maintain state that might change during the lifetime of the widget. Implementing a stateful widget requires at least two classes: 1) a StatefulWidget class that creates an instance of 2) a State class. The StatefulWidget class is, itself, immutable, but the State class persists over the lifetime of the widget.
*/
//To achieve this, wrap the Text widget in a StatefulWidget and update it when the user clicks the button.
class RandomWords extends StatefulWidget {
  @override
  createState() => RandomWordsState();

  /*@override
  State<StatefulWidget> createState() {
    return RandomWordsState();
  }*/
}

//Most of the app’s code resides in this class, which maintains the state for the RandomWords widget.
//This class will save the generated word pairs, which grow infinitely as the user scrolls, and also favorite word pairs, as the user adds or removes them from the list by toggling the heart icon.
class RandomWordsState extends State<RandomWords> {
  //_ private
  final _suggestions = <WordPair>[];

  final _saved = Set<WordPair>();

  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    //final wordPair = WordPair.random();
    //return Text(wordPair.asPascalCase);
    return Scaffold(
      appBar: AppBar(
        title: Text('Startup Name Generator'),
        actions: <Widget>[
          IconButton(
              icon: Icon(Icons.list),
              onPressed: _pushSaved)
        ],
      ),
      body: _buildSuggestions(),
    );
  }

  //When the user taps the list icon in the app bar, build a route and push it to the Navigator’s stack. This action changes the screen to display the new route.
  void _pushSaved() {
    Navigator.of(context).push(
        //Add the call to Navigator.push, as shown by the highlighted code, which pushes the route to the Navigator’s stack.
      MaterialPageRoute(
          builder: (context){
            final tiles = _saved.map(
                (pair){
                  return ListTile(
                    title: Text(
                      pair.asPascalCase,
                      style: _biggerFont,
                    ),
                  );
                },
            );
            final divided = ListTile.
              divideTiles(
                context: context,
                tiles: tiles)
              .toList();

            return Scaffold(
              appBar: AppBar(
                title: Text('Saved Suggestions'),
              ),
              body: ListView(children: divided,),
            );
          },
      ),
    );
  }

  Widget _buildSuggestions(){
    return ListView.builder(
        //padding: const EdgeInsets.all(16.0),
        padding: EdgeInsets.only(left: 10.0, right: 10.0),

        // The itemBuilder callback is called once per suggested word pairing,
        // and places each suggestion into a ListTile row.
        // For even rows, the function adds a ListTile row for the word pairing.
        // For odd rows, the function adds a Divider widget to visually
        // separate the entries. Note that the divider may be difficult
        // to see on smaller devices.
        itemBuilder: (context, i){
          // Add a one-pixel-high divider widget before each row in theListView.
          if (i.isOdd) return Divider();

          // The syntax "i ~/ 2" divides i by 2 and returns an integer result.
          // For example: 1, 2, 3, 4, 5 becomes 0, 1, 1, 2, 2.
          // This calculates the actual number of word pairings in the ListView,
          // minus the divider widgets.
          final index = i ~/ 2;
          // If you've reached the end of the available word pairings...
          if(index >= _suggestions.length){
            // ...then generate 10 more and add them to the suggestions list.
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        }
    );
  }

  //The _buildSuggestions function calls _buildRow once per word pair.
  Widget _buildRow(WordPair suggestion) {
    final alreadySaved = _saved.contains(suggestion);

    return ListTile(
      title: Text(
        suggestion.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      //In Flutter’s react style framework, calling setState() triggers a call to the build() method for the State object, resulting in an update to the UI.
      onTap: (){
        setState(() {
          if(alreadySaved){
            _saved.remove(suggestion);
          }else{
            _saved.add(suggestion);
          }
        });
      },
    );
  }
}
